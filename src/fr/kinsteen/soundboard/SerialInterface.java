package fr.kinsteen.soundboard;

import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.fazecast.jSerialComm.SerialPort;

import fr.kinsteen.soundboard.managers.SoundManager;
import fr.kinsteen.soundboard.managers.VoicemeeterManager;
import fr.kinsteen.soundboard.objects.Action;
import fr.kinsteen.soundboard.objects.Sound;

public class SerialInterface implements Runnable {
	
	private Mixer mixer;
	private SysTray tray;
	private SerialPort comPort;
	private String portName = "";
	private SerialPort portList[];
	
	private Thread thread = null;
	
	// Managers
	public SoundManager soundManager;
	public VoicemeeterManager vmManager;
	private ActionListener reconnectListener;
	private boolean canRead = false;
	
	public SerialInterface(Mixer mixer, SysTray tray) {
		this.mixer = mixer;
		this.tray = tray;
		this.soundManager = new SoundManager(mixer);
		this.vmManager = new VoicemeeterManager();
		
		for (Action a : mixer.getActions()) {
			if (a.action.equalsIgnoreCase("sound")) {
				Sound s = new Sound();
				s.file = a.parameters[0];
				s.trigger = a.trigger;
				this.soundManager.loadClip(s);
			}
		}
		
		this.reconnectListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tray.remove(1);
				startThread();
			}
		};
		
		startThread();
	}
	
	public void quit() {
		vmManager.logout();
	}
	
	/**
	 * This function will try to connect to a serial port provided in argument.
	 * If the port is available, and we can connect to it, the connection will be saved, and we can run the listening Thread.
	 * @param port
	 * @return boolean True if connection is successful, false if there was a problem.
	 */
	public boolean connect(SerialPort port) {
		port.openPort();
		if (port.bytesAvailable() == -1) {
			port.closePort();
			return false;
		} else {
			this.comPort = port;
			this.portName = this.comPort.getSystemPortName();
			this.canRead = true;
			return true;
		}
	}
	
	public String getPortName() {
		return portName;
	}
	
	public SerialPort[] getPortList() {
		this.portList = SerialPort.getCommPorts();
		return portList;
	}
	
	public void findAndConnectPort() {
		SerialPort portList[] = this.getPortList();
		
		if (portList.length == 0) {
			tray.updateStatus("No port found to connect to.");
			tray.updateIcon("iconError");
			tray.addItem("Reconnect ?", reconnectListener, 1);
			System.err.println("No port found to connect to.");
		} else if (portList.length == 1) {
			if (this.connect(this.getPortList()[0])) {
				tray.updateStatus("Ready!");
				tray.updateIcon("icon");
				System.out.println("Opened port " + this.getPortName());
				
				tray.updateVMStatus("Logged in!");
				
				new Thread("MessageDialogThread") { // Non blocking
				    public void run() {
				        JFrame frame = new JFrame("My dialog asks...."); // Have icon in taskbar
				        
				        URL url = Main.class.getResource("/res/icon.png");
					    Image image = Toolkit.getDefaultToolkit().createImage(url);
					    
				        frame.setIconImage(image);
				        frame.setUndecorated(true);
				        frame.setVisible(true);
				        frame.setLocationRelativeTo(null);
						JOptionPane.showMessageDialog(frame, "Connected to " + getPortName() + "!");
				        frame.dispose();
				    }
				}.start();
			} else {
				System.err.println("Port " + portList[0] + " may be busy.");
				tray.updateStatus("Can't connect. Port may be busy.");
				tray.updateIcon("iconError");
				tray.addItem("Reconnect ?", reconnectListener, 1);
				canRead = false;
			}
		} else { // There are more ports available.
			tray.updateStatus("Choose the port you want to connect to.");
	        Menu portsMenu = new Menu("Ports :");
	        for (SerialPort sp : portList) {
	        	MenuItem mi = new MenuItem(sp.getSystemPortName());
	        	mi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						connect(sp);
						startThread();
					}
				});
	        	portsMenu.add(mi);
	        }
	        
	        tray.addMenu(portsMenu);
		}
	}
	

	@Override
	public void run() {
		try {
			System.out.println(Thread.currentThread().getName() + ": " + "Running Serial thread...");
			this.findAndConnectPort();
			System.out.println(Thread.currentThread().getName() + ": " + "Found port : " + canRead);
			
			while (mixer.isRunning() && canRead) {
				while (this.comPort.bytesAvailable() == 0 && mixer.isRunning()) {
					Thread.sleep(5);
				}
				
				if (this.comPort.isOpen() == false) {
					System.err.println(Thread.currentThread().getName() + ": " + "Port is disconnected. Can't reconnect.");
				}
				
				if (true) {
					byte[] readBuffer = new byte[this.comPort.bytesAvailable()];
					this.comPort.readBytes(readBuffer, readBuffer.length);
					String message = new String(readBuffer).replace("\n", "").replace("\r", "");
					if (message.length() > 3) {
						String clean = message.split("!")[0];
						for (int i = 0; clean.length() < 1; i++) {
							clean = message.split("!")[i];
						}
						String firstPart = clean.split(":")[0];
						Action action = mixer.getAction(firstPart);
						switch (action.action) {
							case "sound":
								soundManager.playOnce(action.parameters);
								break;
							
							case "VM":
								if (clean.split(":").length > 1) {
									for (String param : action.parameters) {
										vmManager.doAction(param.replace("%a", clean.split(":")[1]));
									}
								} else {
									for (String param : action.parameters) {
										System.out.println(Thread.currentThread().getName() + ": " + "no param to replace");
										vmManager.doAction(param);
									}
								}
								break;
		
							default:
								System.err.println(Thread.currentThread().getName() + ": " + "Action not implemented.");
								mixer.quit();
								break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName() + ": " + "Closing Serial thread.");
	}
	
	public void startThread() {
		if (this.thread == null || !this.thread.isAlive()) {
			this.thread = new Thread(this, "SerialThread");
			this.thread.start();
		} else {
			System.err.println("Thread is already running");
		}
	}
}

package fr.kinsteen.soundboard;

import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Image;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;
import java.util.ArrayList;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

import fr.kinsteen.soundboard.managers.SoundManager;

public class SysTray {
	private SystemTray tray = null;
	private MenuItem statusItem = null;
	private MenuItem vmStatus = null;
	private TrayIcon trayIcon = null;
    private PopupMenu popup = new PopupMenu();
    private ArrayList<CheckboxMenuItem> devicesItems = new ArrayList<CheckboxMenuItem>(); 
	
	public SysTray(fr.kinsteen.soundboard.Mixer mixer) {
		if (SystemTray.isSupported()) {
			System.out.println("Enabling system tray icon...");
		    tray = SystemTray.getSystemTray();
		    // load an image
		    URL url = Main.class.getResource("/res/icon.png");
		    Image image = Toolkit.getDefaultToolkit().createImage(url);
		    
		    statusItem = new MenuItem("Loading...");
		    statusItem.setEnabled(false);
		    popup.add(statusItem);
		    
		    vmStatus = new MenuItem("VM Loading...");
		    vmStatus.setEnabled(false);
		    popup.add(vmStatus);
		    
		    Menu devicesMenu = new Menu("Devices"); 
		    for (Mixer.Info info : SoundManager.getMixers()) {
		    	if (AudioSystem.getMixer(info).isLineSupported(new Line.Info(SourceDataLine.class))) { // This will filter only output device, those we can play to
		    		CheckboxMenuItem deviceItem = new CheckboxMenuItem(info.getName());
			    	deviceItem.addItemListener(new ItemListener() {
						public void itemStateChanged(ItemEvent e) {
							CheckboxMenuItem target = (CheckboxMenuItem) e.getSource();
							target.setState(true);
							for (CheckboxMenuItem item : devicesItems) {
								if (item != target ) {
									item.setState(false);
								}
							}
							mixer.setOutputDevice(target.getLabel());
							mixer.reloadClips();
						}
					});
			    	
			    	if (info.getName().equals(mixer.getOutputDevice())) {
			    		deviceItem.setState(true);
			    	}

		    		devicesItems.add(deviceItem);
			    	devicesMenu.add(deviceItem);
		    	}
		    }
		    
		    popup.add(devicesMenu);
		    
		    ActionListener exitList = new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	mixer.quit();
		        }
		    };
		    MenuItem exitItem = new MenuItem("Exit");
		    exitItem.addActionListener(exitList);
		    popup.add(exitItem);
		    
		    trayIcon = new TrayIcon(image, "Soundboard", popup);
		    trayIcon.setImageAutoSize(true);
		    
		    try {
		        tray.add(trayIcon); // Adding the icon to system tray
		    } catch (AWTException e) {
		        System.err.println(e);
		    }
		} else {
			System.err.println("Sys tray is not supported. Exiting...");
			System.exit(0);
		}
	}
	
	public void updateStatus(String text) {
	    this.statusItem.setLabel(text);
	}
	
	public void updateVMStatus(String text) {
	    this.vmStatus.setLabel("VM:" + text);
	}
	
	public void updateIcon(String name) {
	    URL url = Main.class.getResource("/res/" + name + ".png");
	    Image image = Toolkit.getDefaultToolkit().createImage(url);
	    trayIcon.setImage(image);
	}
	
	public void addItem(String name, ActionListener listener, int index) {
	    MenuItem item = new MenuItem(name);
	    item.addActionListener(listener);
	    popup.insert(item, index);
	}
	
	public void addMenu(Menu menu) {
		popup.add(menu);
	}
	
	public void remove(int index) {
		popup.remove(index);
	}

	public void close() {
		System.out.println("Closing systray");
		this.tray.remove(trayIcon);
	}
	
}

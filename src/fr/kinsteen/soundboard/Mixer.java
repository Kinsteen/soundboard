package fr.kinsteen.soundboard;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fr.kinsteen.soundboard.objects.Action;

public class Mixer {
	private SerialInterface serialManager = null;
	private SysTray tray = null;
	
	private boolean running = true;
	private String outputDevice;
	private JsonObject configData;
	private Action[] actions = null;
	
	public Mixer() {
		this.readConfig();
		this.actions = this.getActions();
		
		this.tray = new SysTray(this); // Creates system tray icon, in a separate thread
		
		this.serialManager = new SerialInterface(this, this.tray); // Create thread reading serial
		
		System.out.println("Startup completed!");
	}
	
	public void readConfig() {
		try {
			System.out.println("Reading commands from config file...");
		    // create Gson instance
		    Gson gson = new Gson();

		    // create a reader
		    Reader reader = Files.newBufferedReader(Paths.get("commands.json"));
		    // convert JSON file to map
		    this.configData = gson.fromJson(reader, JsonObject.class);
		    this.outputDevice = this.configData.get("output").getAsString(); // Get device output as String
		    reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Action[] getActions() {
	    JsonArray arr = this.configData.getAsJsonArray("actions"); // Get array of sounds objects
	    Gson gson = new Gson();
	    Action[] actions = gson.fromJson(arr, Action[].class); // Convert to class
		return actions;
	}
	
	public Action getAction(String trigger) {
		if (actions == null) {
			this.actions = this.getActions();
		}
		for (Action a : this.actions) {
			if (a.trigger.equalsIgnoreCase(trigger)) {
				return a;
			}
		}
		
		return null;
	}
	
	public String getOutputDevice() {
		return this.outputDevice;
	}
	
	public void setOutputDevice(String outputDevice) {
		this.outputDevice = outputDevice;
	}
	
	public boolean isRunning() {
		return this.running;
	}
	
	public void reloadClips() {
		this.serialManager.soundManager.reloadClips();
	}
	
	public void quit() {
		/**
		 * We need to :
		 *   logout from Voicemeeter
		 *   close all loops
		 */
		this.tray.close();
		this.serialManager.quit(); // This will logout from Voicemeeter
		this.running = false;
	}
}

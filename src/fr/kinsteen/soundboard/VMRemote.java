package fr.kinsteen.soundboard;

import com.sun.jna.Library;
import com.sun.jna.ptr.FloatByReference;
import com.sun.jna.ptr.LongByReference;

public interface VMRemote extends Library {
	public long VBVMR_Login();
	public long VBVMR_Logout();
	public long VBVMR_IsParametersDirty();
	public long VBVMR_GetVoicemeeterType(LongByReference type);
	public long VBVMR_GetParameterFloat(String szParamName, FloatByReference pValue);
	public long VBVMR_SetParameterFloat(String szParamName, float Value);
}

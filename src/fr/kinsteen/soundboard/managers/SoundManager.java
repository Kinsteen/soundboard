package fr.kinsteen.soundboard.managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;

import fr.kinsteen.soundboard.objects.Sound;

public class SoundManager {
	private Map<Sound, Clip> clips = new HashMap<Sound, Clip>();
	private Map<Clip, LineListener> listeners = new HashMap<Clip, LineListener>();
	private fr.kinsteen.soundboard.Mixer mixer;
	
	public SoundManager(fr.kinsteen.soundboard.Mixer mixer) {
		this.mixer = mixer;
	}
	
	/**
	 * This function takes a VORBIS encoded ogg audio stream, and converts it in a PCM (Wav format) audio stream.
	 * @param audioInputStream
	 * @return
	 */
	private AudioInputStream convertToPCM(AudioInputStream audioInputStream) {
        AudioFormat m_format = audioInputStream.getFormat();

        if ((m_format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED) &&
            (m_format.getEncoding() != AudioFormat.Encoding.PCM_UNSIGNED))
        {
            AudioFormat targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
                m_format.getSampleRate(), 16,
                m_format.getChannels(), m_format.getChannels() * 2,
                m_format.getSampleRate(), m_format.isBigEndian());

            return AudioSystem.getAudioInputStream(targetFormat, audioInputStream);
        }

        return audioInputStream;
	}
	
	public Mixer.Info getCableInput() {
		Mixer.Info[] infos = AudioSystem.getMixerInfo();
		Mixer.Info mixer = null;
		
		for (Mixer.Info info : infos) {
			if (info.getName().equalsIgnoreCase(this.mixer.getOutputDevice())) {
				mixer = info;
			}
		}
		
		return mixer;
	}
	
	public Clip loadClip(Sound s) {
		Clip clip = null;
		File file = new File(s.file);
		
		try {
			if (file.exists()) {
				AudioInputStream sound = convertToPCM(AudioSystem.getAudioInputStream(file));
				// load the sound into memory (a Clip)
				clip = AudioSystem.getClip(getCableInput());
				clip.open(sound);
				clips.put(s, clip);
			} else {
				System.err.println("File not found : " + s.file);
				return null;
	        }
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}

		System.out.println("Loaded " + s.file + " in memory.");
		return clip;
	}
	
	public void reloadClips() {
		ArrayList<Sound> toLoad = new ArrayList<Sound>();
		for (Map.Entry<Sound, Clip> entry : clips.entrySet()) {
			entry.getValue().close();
			toLoad.add(entry.getKey());
		}
		
		clips = new HashMap<Sound, Clip>(); // Resets clips map
		for (Sound s : toLoad) {
			loadClip(s); // Reloading everything
		}
	}
	
	public void playSound(String file, boolean once) {
		for (Map.Entry<Sound, Clip> entry : clips.entrySet()) {
			if (entry.getValue().isActive()) {
				entry.getValue().stop();
			}
			
			if (entry.getKey().file.equalsIgnoreCase(file)) {
				System.out.println("Found sound " + file + ", playing...");
				Clip clip = entry.getValue();
				clip.setFramePosition(0);
				clip.start();
				
				if (!once) {
					LineListener list = new LineListener() {
						public void update(LineEvent e) {
							Clip context = ((Clip) e.getSource());
							if (e.getType() == LineEvent.Type.STOP) {
								context.setFramePosition(0); // It's rewind time
								context.start();
							}
						}
					};
					
					clip.addLineListener(list);
					listeners.put(clip, list);
				}
			}
		}
	}
	
	public void playSound(String file) {
		playSound(file, false);
	}

	/**
	 * Play once
	 * @param file
	 */
	public void playOnce(String[] file) {
		playSound(file[0], true);
	}
	
	public void stopSound(String file) {
		for (Map.Entry<Sound, Clip> entry : clips.entrySet()) {
			if (entry.getKey().file.equalsIgnoreCase(file)) {
				Clip clip = entry.getValue();
				if (clip != null) {
					clip.removeLineListener(listeners.get(clip));
				}
				
				return;
			}
		}
	}
	
	public static Mixer.Info[] getMixers() {
		return AudioSystem.getMixerInfo();
	}
}

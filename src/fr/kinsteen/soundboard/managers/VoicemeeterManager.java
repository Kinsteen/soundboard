package fr.kinsteen.soundboard.managers;

import com.sun.jna.Native;
import com.sun.jna.ptr.FloatByReference;

import fr.kinsteen.soundboard.VMRemote;

public class VoicemeeterManager {
	VMRemote vmRemote = null;
	
	public VoicemeeterManager() {
		if (System.getProperty("os.arch").equalsIgnoreCase("amd64")) {
	        this.vmRemote = (VMRemote) Native.load("VoicemeeterRemote64", VMRemote.class);
		} else if (System.getProperty("os.arch").equalsIgnoreCase("x32")) {
			this.vmRemote = (VMRemote) Native.load("VoicemeeterRemote", VMRemote.class);
		} else {
			System.err.println("Arch not detected, can't load DLL.");
			System.exit(1);
		}
		
		this.login();
	}
	
	public void login() {
		long result = vmRemote.VBVMR_Login();
		
		if (result != 0) {
			System.err.println("Can't login to Voicemeeter. Error : " + result);
			System.exit(1);
		}
	}
	
	public void logout() {
		vmRemote.VBVMR_Logout();
	}
	
	public float getParam(String paramName) {
		FloatByReference pValue = new FloatByReference();
		long result = vmRemote.VBVMR_GetParameterFloat(paramName, pValue);
		
		if (result != 0) {
			System.err.println("Error on GetParameterFloat : " + result);
			this.logout();
			System.exit(1);
		}
		
		return pValue.getValue();
	}
	
	public void setParam(String paramName, float value) {
		long result = vmRemote.VBVMR_SetParameterFloat(paramName, value);
		
		if (result != 0) {
			System.err.println("Error on SetParameterFloat : " + result);
			this.logout();
			System.exit(1);
		}
	}
	
	public void toggleParamRange(String paramName, float min, float max) {
		this.hasParametersChanged(); // refresh parameters
		if (this.getParam(paramName) == min) {
			this.setParam(paramName, max);
		} else if (this.getParam(paramName) == max) {
			this.setParam(paramName, min);
		} else {
			System.err.println("Param " + paramName + " is not toggleable");
			this.logout();
			System.exit(1);
		}
	}
	
	public void toggleParam(String paramName) {
		this.toggleParamRange(paramName, 0, 1);
	}
	
	public boolean hasParametersChanged() {
		long result = vmRemote.VBVMR_IsParametersDirty();
		
		if (result == 0) {
			return false;
		} else if (result == 1) {
			return true;
		} else {
			System.err.println("Error on IsParametersDirty : " + result);
			this.logout();
			System.exit(1);
		}
		
		return false;
	}

	public void doAction(String parameters) {
		String fun = parameters.split(":")[0];
		switch (fun) {
			case "toggle":
				if (parameters.split(":").length == 4) {
					String paramName = parameters.split(":")[1];
					float min = Float.parseFloat(parameters.split(":")[2]);
					float max = Float.parseFloat(parameters.split(":")[3]);
					this.toggleParamRange(paramName, min, max);
				} else if (parameters.split(":").length == 2) {
					this.toggleParam(parameters.split(":")[1]);
				}
				break;
	
			case "analog":
				int rawValue = Integer.parseInt(parameters.split(":")[1]);
				String paramName = parameters.split(":")[2];
				int min = Integer.parseInt(parameters.split(":")[3]);
				int max = Integer.parseInt(parameters.split(":")[4]);
				
				// The closer we are to 0, the more logarithmic the mapping becomes. As b tends to infinity, it becomes a linear function
				// See : https://www.desmos.com/calculator/6dhikjngzi
				float b = 30; // Get in parameters ?
				float a = (float) (1023/Math.log(1/b*1023+1));
				float nonLinear = (float) (a * Math.log(1/b * rawValue + 1));
				
				float value = nonLinear/1023f * (max-min) + min; // Linear mapping
				/*if (rawValue > 1020) { // Clip to max if really close
					value = max;
				}*/
				this.setParam(paramName, value);
				break;
				
			default:
				System.err.println("VMManager not implemented : " + fun);
				this.logout();
				System.exit(1);
				break;
		}
	}
}
